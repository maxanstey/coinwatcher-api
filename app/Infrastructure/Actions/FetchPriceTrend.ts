import BitcoinSpotPrice from 'App/Models/BitcoinSpotPrice'
import PriceDifference from 'App/Infrastructure/Types/Bitcoin/PriceDifference'
import TrendResponse from 'App/Infrastructure/Types/Http/TrendResponse'
import DateRangeMode from 'App/Infrastructure/Enums/DateRangeMode'
import PriceMode from 'App/Infrastructure/Enums/PriceMode'
import BitcoinBuyPrice from 'App/Models/BitcoinBuyPrice'
import BitcoinSellPrice from 'App/Models/BitcoinSellPrice'

export default class FetchPriceTrend {
  public async execute (
    from: Date,
    to: Date,
    dateRangeMode: DateRangeMode,
    priceMode: PriceMode
  ): Promise<TrendResponse> {
    let filter: string

    switch (dateRangeMode) {
      case DateRangeMode.hour:
      case DateRangeMode.day:
        filter = 'SECOND(created_at) = 0' // once every minute
        break
      case DateRangeMode.week:
        filter = 'MINUTE(created_at) = 0 AND SECOND(created_at) = 0' // once every hour
        break
      case DateRangeMode.month:
        filter = 'HOUR(created_at) = 0 AND SECOND(created_at) = 0' // once every day
        break
      default:
        filter = ''
    }

    let model

    switch (priceMode) {
      case PriceMode.spot:
        model = BitcoinSpotPrice.query()
        break
      case PriceMode.buy:
        model = BitcoinBuyPrice.query()
        break
      case PriceMode.sell:
      default:
        model = BitcoinSellPrice.query()
    }

    type BitcoinPrice = BitcoinSpotPrice|BitcoinBuyPrice|BitcoinSellPrice

    // @ts-ignore
    const prices: Array<BitcoinPrice> = await model.whereBetween(
      'created_at',
      [
        from,
        to,
      ]
    ).whereRaw(filter).orderBy('created_at')

    const differences: Array<PriceDifference> = []

    prices.forEach(
      (price, index): void => {
        const previousNumber: number = prices[index - 1]?.price || price.price
        const currentNumber: number = price.price

        // TODO: lookup from bitcoin_spot_price_changes instead
        const difference: number = (1 - previousNumber / currentNumber) * 100

        differences.push(
          {
            polled_at: price.createdAt,
            previous_price: previousNumber,
            current_price: currentNumber,
            difference,
          }
        )
      }
    )

    const currentPrice: PriceDifference|undefined = differences[differences.length - 1]

    if (currentPrice === undefined) {
      return {
        latest_price: null,
        trend: null,
        historical_prices: [],
      }
    }

    let priceOneDayAgo: PriceDifference|undefined

    for (const price of differences) {
      const differenceInHours: number|undefined = currentPrice.polled_at.diff(
        price.polled_at,
        'hours'
      ).toObject().hours

      if (differenceInHours !== 24) {
        continue
      }

      priceOneDayAgo = price
    }

    if (priceOneDayAgo === undefined) {
      return {
        latest_price: currentPrice,
        trend: null,
        historical_prices: differences,
      }
    }

    const differenceInLastDay = currentPrice.current_price - priceOneDayAgo.current_price

    return {
      latest_price: currentPrice,
      trend: (differenceInLastDay / currentPrice.current_price),
      historical_prices: differences,
    }
  }
}
