const enum PriceMode {
  spot = 'spot',
  buy = 'buy',
  sell = 'sell',
}

export default PriceMode
