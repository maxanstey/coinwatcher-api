const enum DateRangeMode {
  hour = 'hour',
  day = 'day',
  week = 'week',
  month = 'month',
}

export default DateRangeMode
