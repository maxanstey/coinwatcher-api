import BitcoinApiServiceInterface from 'App/Infrastructure/Interfaces/BitcoinApiServiceInterface'
import axios from 'axios'
import PricePoint from 'App/Infrastructure/Types/Bitcoin/PricePoint'

export default class BitcoinApiService implements BitcoinApiServiceInterface {
  public async getCurrentSpotPrice (): Promise<PricePoint> {
    const { data, } = await axios.get('https://api.coinbase.com/v2/prices/BTC-GBP/spot')

    return data.data
  }

  public async getCurrentBuyPrice (): Promise<PricePoint> {
    const { data, } = await axios.get('https://api.coinbase.com/v2/prices/BTC-GBP/buy')

    return data.data
  }

  public async getCurrentSellPrice (): Promise<PricePoint> {
    const { data, } = await axios.get('https://api.coinbase.com/v2/prices/BTC-GBP/sell')

    return data.data
  }
}
