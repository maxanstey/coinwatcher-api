import { DateTime, } from 'luxon'

export default interface PriceDifference {
  polled_at: DateTime,
  previous_price: number,
  current_price: number,
  difference: number,
}
