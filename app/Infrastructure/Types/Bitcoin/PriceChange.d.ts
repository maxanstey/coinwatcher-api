export default interface PriceChange {
  bitcoinSpotPriceId: number,
  difference: number,
  createdAt: string,
}
