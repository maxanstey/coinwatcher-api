export default interface PricePoint {
  base: string,
  currency: string,
  amount: number,
}
