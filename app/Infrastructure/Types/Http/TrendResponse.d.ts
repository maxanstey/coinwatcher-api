import PriceDifference from 'App/Infrastructure/Types/Bitcoin/PriceDifference'

export default interface TrendResponse {
  latest_price: PriceDifference|null,
  trend: number|null,
  historical_prices: Array<PriceDifference>,
}
