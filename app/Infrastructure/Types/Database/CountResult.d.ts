export default interface CountResult {
  'count(*)': number,
}
