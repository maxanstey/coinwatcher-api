import PricePoint from 'App/Infrastructure/Types/Bitcoin/PricePoint'

export default interface BitcoinApiServiceInterface {
  getCurrentSpotPrice(): Promise<PricePoint>

  getCurrentBuyPrice (): Promise<PricePoint>

  getCurrentSellPrice (): Promise<PricePoint>
}
