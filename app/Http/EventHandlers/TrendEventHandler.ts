import DateRangeMode from 'App/Infrastructure/Enums/DateRangeMode'
import { subDays, subHours, subMonths, subWeeks, } from 'date-fns'
import FetchPriceTrend from 'App/Infrastructure/Actions/FetchPriceTrend'
import PriceMode from 'App/Infrastructure/Enums/PriceMode'

export default class TrendEventHandler {
  public async execute (
    dateRangeMode: DateRangeMode,
    priceMode: PriceMode,
    callback: any
  ): Promise<void> {
    const to: Date = new Date()

    let from: Date = subHours(to, 1)

    switch (dateRangeMode) {
      case DateRangeMode.hour:
        from = subHours(to, 1)
        break
      case DateRangeMode.day:
        from = subDays(to, 1)
        break
      case DateRangeMode.week:
        from = subWeeks(to, 1)
        break
      case DateRangeMode.month:
        from = subMonths(to, 1)
        break
    }

    callback(
      {
        data: await new FetchPriceTrend().execute(from, to, dateRangeMode, priceMode),
      }
    )
  }
}
