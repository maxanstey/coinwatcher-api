import { HttpContextContract, } from '@ioc:Adonis/Core/HttpContext'
import BitcoinApiService from '@ioc:App/Infrastructure/Interfaces/BitcoinApiServiceInterface'
import BitcoinApiServiceInterface from 'App/Infrastructure/Interfaces/BitcoinApiServiceInterface'
import PricePoint from 'App/Infrastructure/Types/Bitcoin/PricePoint'

export default class BitcoinInformationController {
  private readonly bitcoinApiService: BitcoinApiServiceInterface

  constructor () {
    this.bitcoinApiService = BitcoinApiService
  }

  public async getSpotPrice ({ response, }: HttpContextContract) {
    const data: PricePoint = await this.bitcoinApiService.getCurrentSpotPrice()

    return response.ok({
      current_price: data,
    })
  }
}
