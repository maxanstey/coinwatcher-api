import { DateTime, } from 'luxon'
import { BaseModel, column, } from '@ioc:Adonis/Lucid/Orm'

export default class BitcoinSpotPriceChange extends BaseModel {
  @column({ isPrimary: true, })
  public id: number

  @column()
  public bitcoinSpotPriceId: number

  @column()
  public difference: number

  @column.dateTime({ autoCreate: true, })
  public createdAt: DateTime
}
