import { DateTime, } from 'luxon'
import { BaseModel, column, } from '@ioc:Adonis/Lucid/Orm'

export default class BitcoinSpotPrice extends BaseModel {
  @column({ isPrimary: true, })
  public id: number

  @column()
  public price: number

  @column.dateTime({ autoCreate: false, })
  public createdAt: DateTime
}
