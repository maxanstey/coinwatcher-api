import { format as sprintf, } from 'util'

type DisplacementType = 'positive'|'negative'

/**
 * @link https://www.warriortrading.com/weighted-moving-average/
 * @link https://blog.earn2trade.com/weighted-moving-average/
 * @link https://www.statisticshowto.com/probability-and-statistics/statistics-definitions/weighted-mean/
 *
 * @param values
 * @param windowWidth
 * @param displacement
 * @param weighted
 */
export const calculateMovingAverages = (
  values: Array<number>,
  windowWidth: number,
  displacement: DisplacementType = 'negative',
  weighted: Boolean = false
): Array<number|null> => {
  if (values.length === 0) {
    return values
  }

  const width = Math.floor(windowWidth)

  if (width !== windowWidth) {
    console.warn(
      sprintf(
        'The windowWidth variable must be a whole number. It has been rounded down from %s to %s.',
        windowWidth,
        width
      )
    )
  }

  if (width >= values.length) {
    return values
  }

  const movingAverages: Array<number|null> = []

  for (let i = 0; i < values.length; i++) {
    let start: number
    let end: number

    switch (displacement) {
      case 'positive':
        start = i
        end = i + width
        break
      case 'negative':
      default:
        start = i - width
        end = i
    }

    // If the window is wider than the current index allows
    if (start < 0 || end > values.length) {
      movingAverages.push(null)

      continue
    }

    let window: Array<number> = values.slice(start, end)

    if (weighted) {
      /** @link https://math.stackexchange.com/a/60579/1016141 */
      const triangleNumber = (width * (width + 1)) / 2

      window = window.map(
        (value, index) => {
          return value * (index + 1 / triangleNumber)
        }
      )
    }

    const sum = window.reduce((a, b) => a + b, 0)

    if (sum === 0) {
      movingAverages.push(0)
    }

    movingAverages.push(sum / width)
  }

  return movingAverages.map(
    (number) => {
      if (number === null) {
        return null
      }

      // @ts-ignore
      return Math.round((number + Number.EPSILON) * 100) / 100
    }
  )
}

/**
 * @link https://corporatefinanceinstitute.com/resources/knowledge/trading-investing/weighted-moving-average-wma/
 * @link https://www.youtube.com/watch?v=lBBVatoav1s
 *
 * @param values
 * @param windowWidth
 * @param displacement
 */
export const calculateWeightedMovingAverages = (
  values: Array<number>,
  windowWidth: number,
  displacement: DisplacementType = 'negative'
): Array<number|null> => {
  return calculateMovingAverages(values, windowWidth, displacement, true)
}
