const enum LoggerCategory {
  general = 'general',
  fetchBitcoinStatistics = 'fetch_bitcoin_statistics',
  calculateBitcoinSpotPriceChanges = 'calculate_bitcoin_spot_price_changes',
}

export default LoggerCategory
