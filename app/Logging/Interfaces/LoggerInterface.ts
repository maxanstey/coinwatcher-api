import LoggerCategory from 'App/Logging/Enums/LoggerCategory'

export default interface LoggerInterface {
  getCategory(): LoggerCategory;

  setCategory (category: LoggerCategory): this;

  success (message: string): void;

  info (message: string): void;

  warning (message: string): void;

  error (message: string): void;

  fatal (message: string): void;
}
