import { Logger as BaseLogger, } from '@poppinss/cliui/build/src/Logger'
import LoggerInterface from 'App/Logging/Interfaces/LoggerInterface'
import Application from '@ioc:Adonis/Core/Application'
import { format as sprintf, } from 'util'
import LoggerCategory from 'App/Logging/Enums/LoggerCategory'

export default class Logger extends BaseLogger implements LoggerInterface {
  private category: LoggerCategory = LoggerCategory.general

  public getCategory (): LoggerCategory {
    return this.category
  }

  public setCategory (category: LoggerCategory): this {
    this.category = category

    return this
  }

  public success (message: string): void {
    super.success(message, Logger.getDateTimeString())

    Logger.outputToFile(message, 'success')
  }

  public info (message: string): void {
    super.info(message, Logger.getDateTimeString())

    Logger.outputToFile(message, 'info')
  }

  public warning (message: string): void {
    super.warning(message, Logger.getDateTimeString())

    Logger.outputToFile(message, 'warning')
  }

  public error (message: string): void {
    super.error(message, Logger.getDateTimeString())

    Logger.outputToFile(message, 'error')
  }

  public fatal (message: string): void {
    super.fatal(message, Logger.getDateTimeString())

    Logger.outputToFile(message, 'fatal')
  }

  private static getDateTimeString (): string {
    const date: string = (new Date()).toISOString()

    return ` ${date} `
  }

  private static outputToFile (message: string, prefix: string): void {
    const fs: Record<string, Function> = require('fs')

    const directory: string = Application.makePath(process.env.LOGS_DIRECTORY || '')

    const writeFile: Function = () => {
      const filename: string = sprintf(
        '%s/adonisjs-%s.log',
        directory,
        new Date().toISOString().split('T')[0]
      )

      const output = `[${Logger.getDateTimeString()}] [ ${prefix} ] ${message}\n`

      fs.appendFile(
        filename,
        output,
        (error: NodeJS.ErrnoException|null) => {
          if (error === null) {
            return
          }

          throw error
        }
      )
    }

    fs.access(
      directory,
      async (error: NodeJS.ErrnoException|null) => {
        if (error !== null) {
          await fs.mkdirSync(directory, { recursive: true, })
        }

        writeFile()
      }
    )
  }
}
