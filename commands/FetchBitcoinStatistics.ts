import { format, } from 'date-fns'
import { DateTime, } from 'luxon'
import { clearTimeout, } from 'timers'
import { format as sprintf, } from 'util'
import { BaseCommand, } from '@adonisjs/core/build/standalone'
import BitcoinApiServiceInterface from 'App/Infrastructure/Interfaces/BitcoinApiServiceInterface'
import PricePoint from 'App/Infrastructure/Types/Bitcoin/PricePoint'
import BitcoinBuyPrice from 'App/Models/BitcoinBuyPrice'
import BitcoinSellPrice from 'App/Models/BitcoinSellPrice'
import { ApplicationContract, } from '@ioc:Adonis/Core/Application'
import { KernelContract, } from '@adonisjs/ace/build/src/Contracts'
import LoggerInterface from 'App/Logging/Interfaces/LoggerInterface'
import LoggerCategory from 'App/Logging/Enums/LoggerCategory'

export default class FetchBitcoinStatistics extends BaseCommand {
  public static commandName: string = 'bitcoin:fetch-statistics'
  public static description: string = 'Fetches Bitcoin statistics via the API service.'
  public static settings: object = {
    loadApp: true,
    stayAlive: true,
  }
  private bitcoinApiService: BitcoinApiServiceInterface
  private secondsTracked: Array<Boolean> = []
  private timeout: ReturnType<typeof setTimeout>
  private log: LoggerInterface

  constructor (application: ApplicationContract, kernel: KernelContract) {
    super(application, kernel)

    this.bitcoinApiService = require('@ioc:App/Infrastructure/Interfaces/BitcoinApiServiceInterface')
    this.log = require('@ioc:App/Infrastructure/Logging/Interfaces/LoggerInterface')

    this.log.setCategory(LoggerCategory.fetchBitcoinStatistics)
  }

  public async run () {
    if (this.timeout !== undefined) {
      clearTimeout(this.timeout)
    }

    this.timeout = setTimeout(() => this.run(), 250)

    const { default: BitcoinSpotPrice, } = await import('App/Models/BitcoinSpotPrice')

    const date: Date = new Date()
    const seconds: number = +format(date, 's')

    if (seconds === 1) {
      this.secondsTracked = []
    }

    if (this.secondsTracked[seconds]) {
      return
    }

    if (seconds % 5 !== 0) {
      return
    }

    this.secondsTracked[seconds] = true

    this.log.info('Fetching price data.')

    try {
      const spotPrice: PricePoint = await this.bitcoinApiService.getCurrentSpotPrice()

      await BitcoinSpotPrice.create(
        {
          price: spotPrice.amount,
          createdAt: DateTime.fromJSDate(date),
        }
      )

      this.log.info(`Fetched spot price (${spotPrice.amount}).`)

      const buyPrice: PricePoint = await this.bitcoinApiService.getCurrentBuyPrice()

      await BitcoinBuyPrice.create(
        {
          price: buyPrice.amount,
          createdAt: DateTime.fromJSDate(date),
        }
      )

      this.log.info(`Fetched buy price (${buyPrice.amount}).`)

      const sellPrice: PricePoint = await this.bitcoinApiService.getCurrentSellPrice()

      await BitcoinSellPrice.create(
        {
          price: sellPrice.amount,
          createdAt: DateTime.fromJSDate(date),
        }
      )

      this.log.info(`Fetched sell price (${sellPrice.amount}).`)
    } catch (error: any) {
      const message = sprintf(
        'An error occurred while attempting to fetch bitcoin statistics: %s',
        error?.message || '(unknown)'
      )

      this.log.fatal(message)

      return
    }
  }
}
