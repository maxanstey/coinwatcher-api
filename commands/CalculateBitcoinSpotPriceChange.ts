import { BaseCommand, } from '@adonisjs/core/build/standalone'
import { DateTime, } from 'luxon'
import PriceChange from 'App/Infrastructure/Types/Bitcoin/PriceChange'
import Database from '@ioc:Adonis/Lucid/Database'
import { format as sprintf, } from 'util'
import LoggerInterface from 'App/Logging/Interfaces/LoggerInterface'
import { ApplicationContract, } from '@ioc:Adonis/Core/Application'
import { KernelContract, } from '@adonisjs/ace/build/src/Contracts'
import LoggerCategory from 'App/Logging/Enums/LoggerCategory'
import CountResult from 'App/Infrastructure/Types/Database/CountResult'

export default class CalculateBitcoinSpotPriceChanges extends BaseCommand {
  public static commandName = 'bitcoin:calculate-changes'

  public static description = 'Calculates the price differences of Bitcoin spot prices for the current day.'

  public static settings = {
    /**
     * Set the following value to true, if you want to load the application
     * before running the command. Don't forget to call `node ace generate:manifest`
     * afterwards.
     */
    loadApp: true,

    /**
     * Set the following value to true, if you want this command to keep running until
     * you manually decide to exit the process. Don't forget to call
     * `node ace generate:manifest` afterwards.
     */
    stayAlive: false,
  }

  private startDate: DateTime

  private endDate: DateTime

  private log: LoggerInterface

  constructor (application: ApplicationContract, kernel: KernelContract) {
    super(application, kernel)

    this.log = require('@ioc:App/Infrastructure/Logging/Interfaces/LoggerInterface')

    this.log.setCategory(LoggerCategory.calculateBitcoinSpotPriceChanges)
  }

  public async run (): Promise<void> {
    this.startDate = DateTime.now().minus({ days: 1, }).startOf('day')
    this.endDate = this.startDate.endOf('day')

    this.log.info(
      sprintf(
        'Calculating Bitcoin spot price changes from %s to %s.',
        this.startDate.toISO(),
        this.endDate.toISO()
      )
    )

    try {
      await this.calculateTwoMinuteAverages()
    } catch (error) {
      this.log.fatal('A fatal error occurred while attempting to calculate Bitcoin spot price changes.')
    }
  }

  private async calculateTwoMinuteAverages (): Promise<void> {
    /** {@link} https://github.com/adonisjs/core/discussions/2523 */
    const { default: BitcoinSpotPrice, } = await import('App/Models/BitcoinSpotPrice')
    const { default: BitcoinSpotPriceChange, } = await import('App/Models/BitcoinSpotPriceChange')

    const prices = await BitcoinSpotPrice.query()
      .whereBetween(
        'created_at',
        [
          this.startDate.toJSDate(),
          this.endDate.toJSDate(),
        ]
      )
      .whereRaw('(MINUTE(created_at) % 2) = 0 AND SECOND(created_at) = 0') // every 2 minutes
      .orderBy('created_at')

    const changesByPercent: Array<PriceChange> = []

    prices.forEach(
      (currentPrice, index) => {
        if (index === 0) {
          return
        }

        const previousPrice: number = prices[index - 1].price * 100
        const difference = previousPrice - (currentPrice.price * 100)

        if (difference === 0) {
          return
        }

        changesByPercent.push(
          {
            bitcoinSpotPriceId: currentPrice.id,
            difference,
            createdAt: currentPrice.createdAt.toISO(),
          }
        )
      }
    )

    this.log.info(
      sprintf(
        'Saving %s non-zero price changes not accounting for already calculated changes.',
        changesByPercent.length
      )
    )

    if (changesByPercent.length === 0) {
      return
    }

    const mappedChanges: string = changesByPercent.map(
      (change) => {
        return sprintf(
          '(%s, %s, "%s")',
          change.bitcoinSpotPriceId,
          change.difference,
          change.createdAt.slice(0, 19).replace('T', ' ')
        )
      }
    ).join(',')

    const oldCount: ReadonlyArray<CountResult> = await Database.query().from(BitcoinSpotPriceChange.table).count('*')

    await Database.rawQuery(
      `INSERT IGNORE INTO ${BitcoinSpotPriceChange.table} (bitcoin_spot_price_id, difference, created_at) VALUES ${mappedChanges}`
    )

    const newCount: ReadonlyArray<CountResult> = await Database.query().from(BitcoinSpotPriceChange.table).count('*')

    this.log.info(
      sprintf(
        '%s records created.',
        newCount[0]['count(*)'] - oldCount[0]['count(*)']
      )
    )
  }
}
