declare module '@ioc:App/Infrastructure/Interfaces/BitcoinApiServiceInterface' {
  import BitcoinApiServiceInterface from 'App/Infrastructure/Interfaces/BitcoinApiServiceInterface'

  const BitcoinApiService: BitcoinApiServiceInterface

  export default BitcoinApiService
}

declare module '@ioc:App/Infrastructure/Logging/Interfaces/LoggerInterface' {
  import LoggerInterface from 'App/Logging/Interfaces/LoggerInterface'

  const Logger: LoggerInterface

  export default Logger
}
