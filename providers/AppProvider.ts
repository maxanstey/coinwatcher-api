import { ApplicationContract, } from '@ioc:Adonis/Core/Application'
import BitcoinApiService from 'App/Infrastructure/Services/BitcoinApiService'
import Logger from 'App/Logging/Logger'

export default class AppProvider {
  constructor (protected app: ApplicationContract) {
  }

  public register () {
    this.app.container.singleton(
      'App/Infrastructure/Interfaces/BitcoinApiServiceInterface',
      () => new BitcoinApiService()
    )

    this.app.container.bind(
      'App/Infrastructure/Logging/Interfaces/LoggerInterface',
      () => new Logger()
    )
  }

  public async boot () {
    // IoC container is ready
  }

  public async ready () {
    if (this.app.environment === 'web') {
      await import('../start/socket')
    }
  }

  public async shutdown () {
    // Cleanup, since app is going down
  }
}
