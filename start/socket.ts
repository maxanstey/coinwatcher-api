import Ws from 'App/Http/Ws'
import { Socket, } from 'socket.io'
import DateRangeMode from 'App/Infrastructure/Enums/DateRangeMode'
import TrendEventHandler from 'App/Http/EventHandlers/TrendEventHandler'
import PriceMode from 'App/Infrastructure/Enums/PriceMode'
import BitcoinApiService from '@ioc:App/Infrastructure/Interfaces/BitcoinApiServiceInterface'

Ws.boot()

const trendEventHandler = new TrendEventHandler()

Ws.io.on('connection', async (socket: Socket): Promise<void> => {
  socket.on(
    'trend',
    (dateRangeMode: DateRangeMode, priceMode: PriceMode, callback) => {
      return trendEventHandler.execute(dateRangeMode, priceMode, callback)
    }
  )

  socket.on(
    'spot',
    async (callback) => callback(await BitcoinApiService.getCurrentSpotPrice())
  )
})
