import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class BitcoinBuyPrices extends BaseSchema {
  protected tableName = 'bitcoin_buy_prices'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')

      table.decimal('price').index()

      table.timestamp('created_at', { useTz: true, })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
