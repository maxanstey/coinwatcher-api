import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class BitcoinPrices extends BaseSchema {
  protected tableName = 'bitcoin_prices'

  public up () {
    this.schema.renameTable(this.tableName, 'bitcoin_spot_prices')
  }

  public async down () {
    this.schema.renameTable('bitcoin_spot_prices', this.tableName)
  }
}
