import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class BitCoinSpotPriceChanges extends BaseSchema {
  protected tableName = 'bitcoin_spot_price_changes'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')

      table.integer('bitcoin_spot_price_id')
        .unsigned()
        .unique()
        .references('id')
        .inTable('bitcoin_prices')

      table.decimal('difference', 6)

      table.timestamp('created_at', { useTz: true, })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
