import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class BitcoinSpotPriceChanges extends BaseSchema {
  protected tableName = 'bitcoin_spot_price_changes'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('difference').alter()
    })
  }

  public async down () {
    this.schema.alterTable(this.tableName, (table) => {
      table.decimal('difference').alter()
    })
  }
}
